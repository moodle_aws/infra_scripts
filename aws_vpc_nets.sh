#!/bin/bash

# Requirements: aws cli installed + IAM user con permisos de infraestructura

PREFIX=moodle-

usage () { echo "usage" ; exit 1; }
validate_args () {
	# Params: 1 = region, 2 = prefix, 3 = cidr IPv4, 4 = Key Pair, 5 = Amazin AMI ID, 6 = DB_ROOT_PWD, 7 = BASE_DOMAIN
	if [ "$1" = "" ] || [ "$2" = "" ] || [ "$3" = "" ] || [ "$4" = "" ] || [ "$5" = "" ] || [ "$6" = "" ] || [ "$7" = "" ]; then
		usage
	fi
	# Region: must have 3 avail zones
	NUMAVAIL=$(aws ec2 describe-availability-zones --region "$1" --query AvailabilityZones[].ZoneName | sed '1,1d' | sed '$d' | wc -l)
	[ "$NUMAVAIL" -lt "3" ] && echo "Region must have minimum 3 availability zones" && exit 1
	# Prefix: only letters, digits and -, and not start with -
	MATCHES=$(echo "$2" | grep -E "^[a-zA-Z]+(\-*[a-zA-Z0-9]*)*$" | wc -l)
	[ "$MATCHES" = "0" ] && echo "Prefix is not a valid string (start with letter and contains only letters, digits and -)" && exit 1
	# CIDR ^([0-9]{1,3}\.){3}[0-9]{1,3}(\/([0-9]|[1-2][0-9]|3[0-2]))?$
	MATCHES=$(echo "$3" | grep -E "^([0-9]{1,3}\.){3}[0-9]{1,3}(\/([0-9]|[1-2][0-9]|3[0-2]))$" | wc -l)
	[ "$MATCHES" = "0" ] && echo "It is not a correct CIDR" && exit 1
	# Key pair must exist
	OCCURS=$(aws ec2 describe-key-pairs --region "$1" --key-names $4 --query KeyPairs[0].KeyName | wc -l)
	[ "$OCCURS" = "0" ] && echo "Key-pair '$4' not found. You must create it manually and save the .pem" && exit 1
	# Amazon AMI ID must exists
	OCCURS=$(aws ec2 describe-images --region "$1" --image-ids $5 --query Images[0].ImageId | wc -l)
	[ ! "$OCCURS" = "0" ] && echo "AMI $5 not found for region $1" && exit 1
	# DB_ROOT_PWD: >=8 chars, <=41 chars, not containing /, " nor @
	MATCHES=$(echo "$6" | grep -E "^([^\/\"\@]){8,41}$")
	[ "$MATCHES" = "0" ] && echo "Root DB pwd not matched (>=8 chars, <=41 chars, not containing /, \" nor @)" && exit 1
	# BASE_DOMAIN => must exist in AWS Route 53
	OCCURS=$(aws route53 list-hosted-zones --region "$1" --query HostedZones[].Name | grep $7 | wc -l)
	[ ! "$OCCURS" = "0" ] && echo "There is no Route53 hosted zone for domain $7 in region $1" && exit 1
}
	
# Promtps
SOME_KEY=$(aws configure get aws_access_key_id | wc -l)
SOME_SEC=$(aws configure get aws_secret_access_key | wc -l)
[ "$SOME_KEY" = "0" ] && [ "$SOME_SEC" = "0" ] && aws configure
REGION=$(aws configure get region | wc -l)
[ "$REGION" = "0" ] && echo -n "Insert destination region: " && read REGION
echo -n "Insert tags prefix [default '$PREFIX' if you press intro]: " ; read PRF; [ ! "$PRF" = "" ] && PREFIX=$PRF
echo -n "Insert source CIDR IPv4 for admins (eg: 92.191.16.9/32): " ; read USER_IP4_RANGE
COMMON_PARAMS="--region $REGION --output text"
echo -n "Insert key-pair name for instances: " ; read KEY_PAIR
echo -n "Insert the Amazon LINUX AMI ID for your region (HVM, SSD Volume Type...): " ; read AMAZON_AMI_ID
echo -n "Insert the root password for RDS cluster (8 chars min.): " ; read DB_ROOT_PWD
echo -n "Insert your base domain (eg: mpenolms.conf): " ; read BASE_DOMAIN;

validate_args $REGION $PREFIX $USER_IP4_RANGE $KEY_PAIR $AMAZON_AMI_ID $DB_ROOT_PWD $BASE_DOMAIN

AZ1=${REGION}a ; AZ2=${REGION}b ; AZ3=${REGION}c

tag_name () {
	# Args: 1 = object-id, 2 = insert 'moodle' prefix (boolean, true by default), 3 = tag value (optional)
	[ "$1" = "" ] && return 0 
	TAG_VAL=$1 ; [ ! "$3" = "" ] && TAG_VAL=$3
	PREFIX_TMP=$PREFIX ; [ "$2" = "false" ] && PREFIX_TMP=
	aws ec2 create-tags $COMMON_PARAMS --resources $1 --tags Key=Name,Value=${PREFIX_TMP}${TAG_VAL}
}

#### 1) VPC creation
OUTPUT=$(aws ec2 create-vpc $COMMON_PARAMS --cidr-block 10.0.0.0/16)
VPC_ID=$(echo "$OUTPUT" | awk '{print $7}') ; tag_name $VPC_ID
aws ec2 modify-vpc-attribute $COMMON_PARAMS --vpc-id $VPC_ID --enable-dns-support
aws ec2 modify-vpc-attribute $COMMON_PARAMS --vpc-id $VPC_ID --enable-dns-hostnames

# Subnets creation
OUTPUT=$(aws ec2 create-subnet $COMMON_PARAMS --vpc-id $VPC_ID --cidr-block 10.0.0.0/24 --availability-zone $AZ1)
NET1_ID=$(echo "$OUTPUT" | awk '{print $9}') ; tag_name $NET1_ID false "net1-$NET1_ID"
OUTPUT=$(aws ec2 create-subnet $COMMON_PARAMS --vpc-id $VPC_ID --cidr-block 10.0.1.0/24 --availability-zone $AZ1)
NET2_ID=$(echo "$OUTPUT" | awk '{print $9}') ; tag_name $NET2_ID false "net2-$NET2_ID"
OUTPUT=$(aws ec2 create-subnet $COMMON_PARAMS --vpc-id $VPC_ID --cidr-block 10.0.2.0/24 --availability-zone $AZ2)
NET3_ID=$(echo "$OUTPUT" | awk '{print $9}') ; tag_name $NET3_ID false "net3-$NET3_ID"
OUTPUT=$(aws ec2 create-subnet $COMMON_PARAMS --vpc-id $VPC_ID --cidr-block 10.0.3.0/24 --availability-zone $AZ2)
NET4_ID=$(echo "$OUTPUT" | awk '{print $9}') ; tag_name $NET4_ID false "net4-$NET4_ID"
OUTPUT=$(aws ec2 create-subnet $COMMON_PARAMS --vpc-id $VPC_ID --cidr-block 10.0.4.0/24 --availability-zone $AZ3)
NET5_ID=$(echo "$OUTPUT" | awk '{print $9}') ; tag_name $NET5_ID false "net5-$NET5_ID"

# Internet Gateway + Elastic IP + Nat gateway in NET1, the public net
OUTPUT=$(aws ec2 create-internet-gateway $COMMON_PARAMS)
IG_ID=$(echo "$OUTPUT" | awk '{print $2}') ; tag_name $IG_ID
aws ec2 attach-internet-gateway $COMMON_PARAMS --internet-gateway-id $IG_ID --vpc-id $VPC_ID
OUTPUT=$(aws ec2 allocate-address $COMMON_PARAMS --domain vpc)
EIP_ID=$(echo "$OUTPUT" | awk '{print $1}')
OUTPUT=$(aws ec2 create-nat-gateway $COMMON_PARAMS --subnet-id $NET1_ID --allocation-id $EIP_ID)
NG_ID=$(echo "$OUTPUT" | awk '{print $3}')
# Nat gateways don't support tags

# Route tables creation
RT1_pub_ID=$(aws ec2 describe-route-tables $COMMON_PARAMS --filters Name=association.main,Values=true | grep ASSOCIATIONS | tail -n 1 | awk '{print $4}')
tag_name $RT1_pub_ID
aws ec2 create-route $COMMON_PARAMS --route-table-id $RT1_pub_ID --destination-cidr-block 0.0.0.0/0 --gateway-id $IG_ID
aws ec2 associate-route-table $COMMON_PARAMS --route-table-id $RT1_pub_ID --subnet-id $NET1_ID
aws ec2 associate-route-table $COMMON_PARAMS --route-table-id $RT1_pub_ID --subnet-id $NET2_ID
aws ec2 associate-route-table $COMMON_PARAMS --route-table-id $RT1_pub_ID --subnet-id $NET3_ID

OUTPUT=$(aws ec2 create-route-table $COMMON_PARAMS --vpc-id $VPC_ID | head -n 1) # despues de la primera fila hay una fila mas por cada route
RT2_priv_ID=$(echo "$OUTPUT" | awk '{print $2}') ; tag_name $RT2_priv_ID
aws ec2 create-route $COMMON_PARAMS --route-table-id $RT2_priv_ID --destination-cidr-block 0.0.0.0/0 --nat-gateway-id $NG_ID
aws ec2 associate-route-table $COMMON_PARAMS --route-table-id $RT2_priv_ID --subnet-id $NET4_ID
aws ec2 associate-route-table $COMMON_PARAMS --route-table-id $RT2_priv_ID --subnet-id $NET5_ID

# New security Groups
SG_SSH_PUB="${PREFIX}SSH_PUB"
SG_WEB_PUB="${PREFIX}WEB_PUB"
SG_SSH_INT="${PREFIX}SSH_INT"
SG_MYSQL_INT="${PREFIX}MYSQL_INT"
SG_NFS_INT="${PREFIX}NFS_INT"

SG_SSH_PUB_ID=$(aws ec2 create-security-group $COMMON_PARAMS --group-name $SG_SSH_PUB --description $SG_SSH_PUB --vpc-id $VPC_ID)
tag_name "$SG_SSH_PUB_ID"
ACL="[{\"IpProtocol\": \"tcp\", \"FromPort\": 22, \"ToPort\": 22, \"IpRanges\": [{\"CidrIp\": \"$USER_IP4_RANGE\"}]}]"
aws ec2 authorize-security-group-ingress $COMMON_PARAMS --group-id $SG_SSH_PUB_ID --ip-permissions "$ACL"

SG_WEB_PUB_ID=$(aws ec2 create-security-group $COMMON_PARAMS --group-name $SG_WEB_PUB --description $SG_WEB_PUB --vpc-id $VPC_ID)
tag_name "$SG_WEB_PUB_ID"
ACL="[{\"IpProtocol\": \"tcp\", \"FromPort\": 80, \"ToPort\": 80, \"IpRanges\": [{\"CidrIp\": \"0.0.0.0/0\"}]}]"
aws ec2 authorize-security-group-ingress $COMMON_PARAMS --group-id $SG_WEB_PUB_ID --ip-permissions "$ACL"
ACL="[{\"IpProtocol\": \"tcp\", \"FromPort\": 443, \"ToPort\": 443, \"IpRanges\": [{\"CidrIp\": \"0.0.0.0/0\"}]}]"
aws ec2 authorize-security-group-ingress $COMMON_PARAMS --group-id $SG_WEB_PUB_ID --ip-permissions "$ACL"

SG_SSH_INT_ID=$(aws ec2 create-security-group $COMMON_PARAMS --group-name $SG_SSH_INT --description $SG_SSH_INT --vpc-id $VPC_ID)
tag_name "$SG_SSH_INT_ID"
ACL="[{\"IpProtocol\": \"tcp\", \"FromPort\": 22, \"ToPort\": 22, \"IpRanges\": [{\"CidrIp\": \"10.0.0.0/16\"}]}]"
aws ec2 authorize-security-group-ingress $COMMON_PARAMS --group-id $SG_SSH_INT_ID --ip-permissions "$ACL"
aws ec2 authorize-security-group-ingress $COMMON_PARAMS --group-id $SG_SSH_INT_ID --source-group $SG_WEB_PUB_ID --protocol tcp --port 22
aws ec2 authorize-security-group-ingress $COMMON_PARAMS --group-id $SG_SSH_INT_ID --source-group $SG_SSH_PUB_ID --protocol tcp --port 22

SG_MYSQL_INT_ID=$(aws ec2 create-security-group $COMMON_PARAMS --group-name $SG_MYSQL_INT --description $SG_MYSQL_INT --vpc-id $VPC_ID)
tag_name "$SG_MYSQL_INT_ID"
ACL="[{\"IpProtocol\": \"tcp\", \"FromPort\": 3306, \"ToPort\": 3306, \"IpRanges\": [{\"CidrIp\": \"10.0.0.0/16\"}]}]"
aws ec2 authorize-security-group-ingress $COMMON_PARAMS --group-id $SG_MYSQL_INT_ID --ip-permissions "$ACL"
aws ec2 authorize-security-group-ingress $COMMON_PARAMS --group-id $SG_MYSQL_INT_ID --source-group $SG_WEB_PUB_ID --protocol tcp --port 3306
aws ec2 authorize-security-group-ingress $COMMON_PARAMS --group-id $SG_MYSQL_INT_ID --source-group $SG_SSH_PUB_ID --protocol tcp --port 3306

SG_NFS_INT_ID=$(aws ec2 create-security-group $COMMON_PARAMS --group-name $SG_NFS_INT --description $SG_NFS_INT --vpc-id $VPC_ID)
tag_name "$SG_NFS_INT_ID"
ACL="[{\"IpProtocol\": \"tcp\", \"FromPort\": 0, \"ToPort\": 65535, \"IpRanges\": [{\"CidrIp\": \"10.0.0.0/16\"}]}]"
aws ec2 authorize-security-group-ingress $COMMON_PARAMS --group-id $SG_NFS_INT_ID --ip-permissions "$ACL"
aws ec2 authorize-security-group-ingress $COMMON_PARAMS --group-id $SG_NFS_INT_ID --source-group $SG_WEB_PUB_ID --protocol tcp --port "0-65535"
aws ec2 authorize-security-group-ingress $COMMON_PARAMS --group-id $SG_NFS_INT_ID --source-group $SG_SSH_PUB_ID --protocol tcp --port "0-65535"


#### 2) New MySQL with multi-az backup replica
DB_NET_GRP=$(aws rds create-db-subnet-group --region $REGION --db-subnet-group-name ${PREFIX}RDS_NET_GRP --db-subnet-group-description ${PREFIX}RDS_NET_GRP --subnet-ids $NET4_ID $NET5_ID --query DBSubnetGroup.DBSubnetGroupName | tr -d "\"")
DB_PRM_GRP=$(aws rds create-db-parameter-group --region $REGION --db-parameter-group-name ${PREFIX}RDS-PARAM-GRP --description ${PREFIX}RDS_PARM_GRP --db-parameter-group-family "MySQL5.7" --query DBParameterGroup.DBParameterGroupName | tr -d "\"")
DB_MASTER_ID=$(aws rds create-db-instance --region $REGION --db-instance-identifier ${PREFIX}RDS-INST1 --engine mysql --engine-version 5.7.17 \
 --master-username root --master-user-password $DB_ROOT_PWD --db-instance-class db.t2.medium --db-subnet-group-name $DB_NET_GRP --db-parameter-group-name $DB_PRM_GRP \
 --vpc-security-group-ids $SG_MYSQL_INT_ID $SG_SSH_INT_ID --auto-minor-version-upgrade --no-publicly-accessible \
 --backup-retention-period 3 --preferred-backup-window 03:30-04:30 --preferred-maintenance-window Sat:04:40-Sat:08:40 \
 --allocated-storage 500 --copy-tags-to-snapshot --tags Key=Name,Value=${PREFIX}RDS-INS --query DBInstance.DBInstanceIdentifier \
 --multi-az | tr -d "\"")
 #--availability-zone $AZ2 #incompatible with multi-az
# Read replica discarded due to async replication (causing errors when expected reads not found)
#echo "Waiting for DB instance availability..."
#IS_AVAIL=0 ; NUM_ITERS=0
#while [ "$IS_AVAIL" = "0" ] && [ "$NUM_ITERS" -lt "30" ]
#do
#        [ "$NUM_ITERS" -gt "0" ] && sleep 15
#        IS_AVAIL=$(aws rds describe-db-instances --region $REGION --db-instance-identifier $DB_MASTER_ID --query DBInstances[0].DBInstanceStatus | grep available | wc -l)
#        NUM_ITERS="$(expr $NUM_ITERS + 1)"
#done
#aws rds create-db-instance-read-replica --region $REGION --db-instance-identifier ${PREFIX}RDS-INST2 --source-db-instance-identifier $DB_MASTER_ID

# New Aurora cluster (discarded due to Moodle 3.2 DDL incompatibilities)
#DB_CLST_NET_GRP=$(aws rds create-db-subnet-group --region $REGION --db-subnet-group-name ${PREFIX}RDS_NET_GRP --db-subnet-group-description ${PREFIX}RDS_NET_GRP --subnet-ids $NET4_ID $NET5_ID --query DBSubnetGroup.DBSubnetGroupName | tr -d "\"")
#DB_PRM_GRP=$(aws rds create-db-parameter-group --region $REGION --db-parameter-group-name ${PREFIX}RDS-PARM-GRP --description ${PREFIX}RDS_PARM_GRP --db-parameter-group-family "aurora5.6" --query DBParameterGroup.DBParameterGroupName | tr -d "\"")
#DB_CLST_PRM_GRP=$(aws rds create-db-cluster-parameter-group --region $REGION --db-cluster-parameter-group-name ${PREFIX}RDS-CPARM-GRP --description ${PREFIX}RDS_PARM_GRP --db-parameter-group-family "aurora5.6" --query DBClusterParameterGroup.DBClusterParameterGroupName |  tr -d "\"")
#DB_CLST_ID=$(aws rds create-db-cluster --region $REGION --db-cluster-identifier ${PREFIX}RDS-CLUSTER --engine aurora --master-username root --master-user-password $DB_ROOT_PWD \
#         --db-subnet-group-name $DB_CLST_NET_GRP --db-cluster-parameter-group-name $DB_CLST_PRM_GRP --vpc-security-group-ids $SG_MYSQL_INT_ID $SG_SSH_INT_ID --availability-zones "$AZ2" "$AZ3" \
#         --backup-retention-period 3 --preferred-backup-window 03:30-04:30 --preferred-maintenance-window Sat:04:40-Sat:08:40 --tags Key=Name,Value=${PREFIX}RDS_CLUSTER --no-storage-encrypted \
#         --query DBCluster.DBClusterIdentifier | tr -d "\"")
#echo "Waiting for DB Cluster availability..."
#IS_AVAIL=0 ; NUM_ITERS=0
#while [ "$IS_AVAIL" = "0" ] && [ "$NUM_ITERS" -lt "30" ]
#do
#        [ "$NUM_ITERS" -gt "0" ] && sleep 15
#        IS_AVAIL=$(aws rds describe-db-clusters --region $REGION --db-cluster-identifier $DB_CLST_ID --query DBClusters[0].Status | grep available | wc -l)
#        NUM_ITERS="$(expr $NUM_ITERS + 1)"
#done
## Aurora primary isntance (first instance created will be always the primary)
#aws rds create-db-instance --region $REGION --db-instance-identifier ${PREFIX}RDS-INST1 --db-cluster-identifier $DB_CLST_ID --engine aurora --engine-version 5.6 \
#        --db-instance-class db.t2.medium --availability-zone $AZ2 --db-parameter-group-name $DB_PRM_GRP \
#        --auto-minor-version-upgrade --no-publicly-accessible --tags Key=Name,Value=${PREFIX}RDS-INST1
## Aurora replica instance (RDS automatically detects existence of the primary)
#aws rds create-db-instance --region $REGION --db-instance-identifier ${PREFIX}RDS-INST2 --db-cluster-identifier $DB_CLST_ID --engine aurora --engine-version 5.6 \
#        --db-instance-class db.t2.medium --availability-zone $AZ3 --db-parameter-group-name $DB_PRM_GRP \
#        --auto-minor-version-upgrade --no-publicly-accessible --tags Key=Name,Value=${PREFIX}RDS-INST2


#### 3) New S3 bucket for moodledata backups (without lifecycle because it is for files incremental sync purpouse)
PREFIX_LOWER=$(echo $PREFIX | tr '[:upper:]' '[:lower:]')
aws s3 mb $COMMON_PARAMS s3://${PREFIX_LOWER}moodledata | awk '{print $2}'

# New S3 bucket for sql backups
# 1 folder for daily (10 days of life), 1 folder for weekly (transitions to glacier and 2 years of life)
aws s3 mb $COMMON_PARAMS s3://${PREFIX_LOWER}sql-backups
aws s3api put-object $COMMON_PARAMS --bucket ${PREFIX_LOWER}sql-backups --key daily/
aws s3api put-object $COMMON_PARAMS --bucket ${PREFIX_LOWER}sql-backups --key weekly/
aws s3api put-bucket-lifecycle-configuration $COMMON_PARAMS --bucket ${PREFIX_LOWER}sql-backups --lifecycle-configuration "{\"Rules\":[{\"ID\":\"Rotate weekly SQLs to Glacier\",\"Prefix\":\"weekly/\",\"Status\":\"Enabled\",\"Transitions\":[{\"Days\": 2,\"StorageClass\": \"GLACIER\"}],\"Expiration\":{\"Days\":730}},{\"ID\":\"Remove daily SQLs\",\"Prefix\":\"daily/\",\"Status\":\"Enabled\",\"Expiration\":{\"Days\": 10}}]}"

# New S3 bucket for VHosts and node configs
aws s3 mb $COMMON_PARAMS s3://${PREFIX_LOWER}configs
aws s3api put-object $COMMON_PARAMS --bucket ${PREFIX_LOWER}configs --key apache_confd/
cp templates/apache/00-apache_cross_maintenance.conf.hidden /tmp/00-apache_cross_maintenance.conf.hidden
cp templates/apache/XX.domain80.conf /tmp/01.$BASE_DOMAIN.80.conf
cp templates/apache/XX.domain.conf /tmp/02.defaultblank.$BASE_DOMAIN.443.conf
BASE_DOMAIN_SLASH1=$(echo $BASE_DOMAIN | sed 's/\./\\\./g')
BASE_DOMAIN_SLASH2=$(echo $BASE_DOMAIN | sed 's/\./\\\\\./g')
sed -i "s/\[port\]/443/g" /tmp/00-apache_cross_maintenance.conf.hidden
sed -i "s/\[basedomain\]/${BASE_DOMAIN_SLASH1}/g" /tmp/00-apache_cross_maintenance.conf.hidden
sed -i "s/\[domain\]/${BASE_DOMAIN_SLASH1}/g" /tmp/01.$BASE_DOMAIN.80.conf
sed -i "s/\[domain\]/${BASE_DOMAIN_SLASH1}/g" /tmp/02.defaultblank.$BASE_DOMAIN.443.conf
sed -i "s/\[domain\_slash\]/${BASE_DOMAIN_SLASH2}/g" /tmp/01.$BASE_DOMAIN.80.conf
sed -i "s/\[domain\_slash\]/${BASE_DOMAIN_SLASH2}/g" /tmp/02.defaultblank.$BASE_DOMAIN.443.conf
sed -i "s/\[port\]/443/g" /tmp/02.defaultblank.$BASE_DOMAIN.443.conf
aws s3api put-object $COMMON_PARAMS --bucket ${PREFIX_LOWER}configs --key apache_confd/00-apache_cross_maintenance.conf.hidden --body /tmp/00-apache_cross_maintenance.conf.hidden
aws s3api put-object $COMMON_PARAMS --bucket ${PREFIX_LOWER}configs --key apache_confd/01.$BASE_DOMAIN.80.conf --body /tmp/01.$BASE_DOMAIN.80.conf
aws s3api put-object $COMMON_PARAMS --bucket ${PREFIX_LOWER}configs --key apache_confd/02.defaultblank.$BASE_DOMAIN.443.conf --body /tmp/02.defaultblank.$BASE_DOMAIN.443.conf

#### 4) New EFS end point for moodledata shared
EFS_ID=$(aws efs create-file-system $COMMON_PARAMS --performance-mode generalPurpose --creation-token ${PREFIX}-Shared-NFS | head -n 1 | awk '{print $3}')
aws efs create-tags $COMMON_PARAMS --file-system-id $EFS_ID --tags Key=Name,Value=${PREFIX}${EFS_ID}
echo "Waiting for EFS availability..."
IS_AVAIL=0 ; NUM_ITERS=0
while [ "$IS_AVAIL" = "0" ] && [ "$NUM_ITERS" -lt "30" ]
do
	[ "$NUM_ITERS" -gt "0" ] && sleep 15
	IS_AVAIL=$(aws efs describe-file-systems --region $REGION --file-system-id $EFS_ID --query 'FileSystems[0].LifeCycleState' | grep available | wc -l)
	NUM_ITERS="$(expr $NUM_ITERS + 1)"
done
if [ ! "$IS_AVAIL" = "0" ] ; then
	aws efs create-mount-target $COMMON_PARAMS --file-system-id $EFS_ID --subnet-id $NET2_ID --security-groups $SG_NFS_INT_ID
	aws efs create-mount-target $COMMON_PARAMS --file-system-id $EFS_ID --subnet-id $NET3_ID --security-groups $SG_NFS_INT_ID
else
	echo "Problems with EFS creation."
fi


#### 5) New AWS Tools Instance (Amazon Linux AMI = Bastion)

OUTPUT=$(aws ec2 allocate-address $COMMON_PARAMS --domain vpc)
EIP2_ID=$(echo "$OUTPUT" | awk '{print $1}')
BASTION_ID=$(aws ec2 run-instances $COMMON_PARAMS --image-id $AMAZON_AMI_ID --instance-type t2.small --count 1 --disable-api-termination --subnet-id $NET1_ID --placement AvailabilityZone=$AZ1,Tenancy=default --block-device-mappings "DeviceName=/dev/xvdb,Ebs={VolumeSize=50,DeleteOnTermination=true,VolumeType=gp2}" --security-group-ids $SG_SSH_PUB_ID --key-name $KEY_PAIR --query 'Instances[0].InstanceId')
tag_name "$BASTION_ID" false "$PREFIX-bastion-$BASTION_ID"
# TODO en user-data: mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 $EFS_ID.efs.$REGION.amazonaws.com:/ efs-mount-point
# TODO montar el End-Point en algun momento para montar las instancias (sigue siempre una estructura fija de EFS ID + region name)
echo "Waiting for BASTION availability..."
IS_AVAIL=0 ; NUM_ITERS=0
while [ "$IS_AVAIL" = "0" ] && [ "$NUM_ITERS" -lt "30" ]
do
	[ "$NUM_ITERS" -gt "0" ] && sleep 15
	IS_AVAIL=$(aws ec2 describe-instances --region $REGION --instance-id $BASTION_ID --query 'Reservations[0].Instances[0].State.Name' | grep running | wc -l)
	NUM_ITERS="$(expr $NUM_ITERS + 1)"
done
aws ec2 associate-address $COMMON_PARAMS --instance-id $BASTION_ID --allocation-id $EIP2_ID


#### 6) New Front Cluster
# New CentOS 7 instance (Front1) => Out of the GroupScalling
	# Con auto asignacion de IP publica, con el IAM Systems Manager Role, 
	# Debe esperar a que esten creadas la DB instance y available, o al menos a que este disponible su End Point

# New LB for domain and security certificate
	# TODO aws route53 change-resource-record-sets --hosted-zone-id $DOUTE53_HOSTED_ZONE_ID --change-batch "{\"Changes\":[{\"Action\":\"CREATE\",\"ResourceRecordSet\":{\"Name\":\"defaultblank.$BASE_DOMAIN\",\"Type\":\"A\",\"AliasTarget\":{\"HostedZoneId\" : \"$LB_HOSTED_ZONE_ID\",\"DNSName\": \"$LB_NAME\",\"EvaluateTargetHealth\":false}}}]}"

# New GroupScalling from ami base for Moodle instances
