#!/bin/bash

# Examples:
#	For a development environment: ./create_tenant.sh -s mix -i tenant1 -b mpenolms.com -h false -n localhost
#	For an AWS front: ./create_tenant.sh -s front -i tenant1 -b mpenolms.com -h true
#	For an AWS bd: ./create_tenant.sh -s bd -i tenant1 -b mpenolms.com -n "10.0.%"

# NOTA IMPORTANTE: Este script es para soluciones de BD no basadas en replicacion master-master, master-slave, etc. Es para soluciones de BD unica simples.

# CONSTANTES
DATADIR=/data
MOODLEDATA=$DATADIR/cluster_shared/moodledatas
CURRENT_PATH=${PWD}
MOODLE_DB_USER='moodleuser'
SUPPORTED_SIDES=(front bd)
BOOL_VALS=(false true)

# VARIABLES
DEFCRONINT="*/5"
SIDE=
IDTENANT=
BASE_DOMAIN=
BEHINDPROXY=false

# FUNCIONES
usage() { echo "Usage: $0 -s <side:front|bd> -i <id-tenant:string> -b <base-domain:string> [-h <behindproxy:true|false>] [-n <bd-moodleuser-netmask:string>]" 1>&2; exit 1; }
mandatoriesargs() { [ ! "$SIDE" = "" ] && [ ! "$IDTENANT" = "" ] && [ ! "$BASE_DOMAIN" = "" ] || usage; }
inarray() {
        # 2 o 3 params: 1 = aguja, 2 = vector pajar, 3 = si la busqueda se hace como starts-with o, si no viene, como coincidencia exacta
        local e

        if [ "$3" = "starts-with" ] ; then
                for e in "${@:2}"; do
                        COND=$(echo "$1" | grep "^$e")
                        [ ! "$COND" = "" ] && return "1";
                done
                return "0"
        fi

        # en otro caso
        for e in "${@:2}"; do [ "$e" = "$1" ] && return "1"; done
        return "0"
}
# We don't want some problematic characters in DB name (.,-)
reformat_domain_syntax() {
	# Params: 1 = base_domain
	REGEXP="^([0-9a-zA-Z]+\.?[0-9a-zA-Z]+\-?[0-9a-zA-Z]+)$"
	MATCH=$(echo "$1" | grep -E "$REGEXP" | wc -l)
	[ "$MATCH" = "0" ] && echo "0" && exit 1
	# Replacing '-' with '__' and '.' with '_'
	DOMAIN_BD=$(echo "$1" | sed 's/\./\_/g' | sed 's/\-/\__/g')
	echo $DOMAIN_BD
}
replace_vhost() {
	# 2 params: 1 = conf_path, 2 = port
	TMPPATH=$1
	sed -i "s/\[port\]/${2}/g" $TMPPATH && sed -i "s/\[domain\]/${IDTENANT}.${BASE_DOMAIN}/g" $TMPPATH
}
new_front_tenant() {
	# 1) Apache VHost creation
	IS_VHOST=$(find /etc/httpd/conf.d -maxdepth 1 -type f | sed -e 's/^\/etc\/httpd\/conf\.d\///g' | grep -P "^(\d)+\..+\.conf" | grep "$IDTENANT.$BASE_DOMAIN" | wc -l | awk '{print $1}')
	if [ "$IS_VHOST" = "0" ] ; then
		INDEX=$(find /etc/httpd/conf.d -maxdepth 1 -type f | sed -e 's/^\/etc\/httpd\/conf\.d\///g' | grep -P "^(\d)+\..+\.conf" | awk -F "." '{print $1}' | tail -1)
		INDEX=$(($INDEX + 1))
		[[ $INDEX < 99 ]] && INDEX=$(printf %02d $INDEX)
		TMPPATH=/etc/httpd/conf.d/${INDEX}.${IDTENANT}.${BASE_DOMAIN}.443.conf
		yes | cp $CURRENT_PATH/templates/apache/XX.domain.conf $TMPPATH && replace_vhost "$TMPPATH" "443" && chmod 644 $TMPPATH
	fi
	# TODO Copiar el nuevo VHost en carpeta especial del EFS / bucket S3 compartido

	# 2) Moodledatas creation
	TMPPATH="$MOODLEDATA/${IDTENANT_BD}_${BASE_DOMAIN_BD}"
	[ ! -d "$TMPPATH" ] && mkdir -p $TMPPATH && chown apache:root $TMPPATH && chmod 770 $TMPPATH

	# 3) Insert full tenant domain int /etc/hosts for future crons
	IS_DOMAIN=$(grep -P "^127\.0\.0\.1.+${IDTENANT}\.${BASE_DOMAIN}.*$" /etc/hosts | wc -l | awk '{print $1}')
	[ "$IS_DOMAIN" = "0" ] && TMPPATH=/etc/hosts && echo "" >> $TMPPATH && echo "127.0.0.1 ${IDTENANT}.${BASE_DOMAIN}" >> $TMPPATH
	
	# 4) Creates moodle crontab
	IS_CRON=$(crontab -l | grep "https://${IDTENANT}.${BASE_DOMAIN}" | wc -l | awk '{print $1}')
	[ "$IS_CRON" = "0" ] && (crontab -l; echo "$DEFCRONINT * * * * wget --no-check-certificate -q -O /dev/null https://${IDTENANT}.${BASE_DOMAIN}/admin/cron.php?password=$MOODLE_CRON_PWD") | crontab -

	# 5) reiniciar apache
	service httpd restart
	
	# TODO Si estamos en AWS:
	# Anadir el dominio del tenant (ej.: tenant2.jastarloa.es) a Route 53 como un A ALIAS del DNS del balanceador => usar AWS CLI
	# 	TODO Pasar como argumentos Hosted Zone de dominio y Hosted Zone de Balacneador (se peude ver en consola web), porque hay inci en AWS CLI que no recupera el segundo.
	#>aws route53 change-resource-record-sets --hosted-zone-id $DOUTE53_HOSTED_ZONE_ID \
	#	--change-batch "{\"Changes\":[{\"Action\":\"CREATE\",\"ResourceRecordSet\":{\"Name\":\"tenant1.jastarloa.es\",\"Type\":\"A\",\"AliasTarget\":{\"HostedZoneId\" : \"$LB_HOSTED_ZONE_ID\",\"DNSName\": \"$LB_NAME\",\"EvaluateTargetHealth\":false}}}]}"
	# Crear la BD en el EndPoint de la BD real
}
new_bd_tenant(){
	local DATABASE="${IDTENANT_BD}_${BASE_DOMAIN_BD}"
	
	export MYSQL_PWD=
	[ ! "$DB_ROOT_PWD" = "" ] && export MYSQL_PWD=$DB_ROOT_PWD
	mysql -u root -e "CREATE DATABASE $DATABASE DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci"
	mysql -u root -e "GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,CREATE TEMPORARY TABLES,DROP,INDEX,ALTER ON $DATABASE.* TO '$MOODLE_DB_USER'@'localhost' IDENTIFIED BY '$MOODLE_DB_USER_PDW'"
	[ ! -z "$BD_MOODLEUSER_NETMASK" ] && mysql -u root -e "GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,CREATE TEMPORARY TABLES,DROP,INDEX,ALTER ON $DATABASE.* TO '$MOODLE_DB_USER'@'$BD_MOODLEUSER_NETMASK' IDENTIFIED BY '$MOODLE_DB_USER_PDW'"
	mysql -u root -e "FLUSH PRIVILEGES"
}
prompt_pwd(){
	# Args: 1 = Subtext message
	local TMP_PWD
	echo -n "Type the $1 password, followed by [ENTER]:" ; read TMP_PWD ; return $TMP_PWD
}

# VALIDACION Y RECUPERACION DE ARGUMENTOS
while getopts ":i:s:h:c:n:b:" opt; do
  case $opt in
		(i) IDTENANT="$OPTARG";;
		(s) SIDE="$OPTARG";;
		(h) BEHINDPROXY="$OPTARG";;
		(n) BD_MOODLEUSER_NETMASK="$OPTARG";;
		(b) BASE_DOMAIN="$OPTARG";;
		(*) usage;;
  esac
done
mandatoriesargs
inarray $SIDE "${SUPPORTED_SIDES[@]}" && [ "$?" = "0" ] && echo "Formato del lado (-s) no es soportado: " && usage
inarray $BEHINDPROXY "${BOOL_VALS[@]}" && [ "$?" = "0" ] && usage

BASE_DOMAIN_BD=$(reformat_domain_syntax $BASE_DOMAIN) ; [ "$BASE_DOMAIN_BD" = "0" ] && echo "[ERROR] Domain is syntactically incorrect" && exit 1
IDTENANT_BD=$(reformat_domain_syntax $IDTENANT) ; [ "$IDTENANT_BD" = "0" ] && echo "[ERROR] Tenant ID is syntactically incorrect " && exit 1

[ "$SIDE" = "bd" ] && [ -z "$DB_ROOT_PWD" ] && echo -n "Type the Mysql root password (for tenant schema creation), followed by [ENTER]:" && read DB_ROOT_PWD
[ "$SIDE" = "bd" ] && [ -z "$MOODLE_DB_USER_PDW" ] && echo -n "Type the MySQL moodleuser password (for Moodle connection), followed by [ENTER]:" && read MOODLE_DB_USER_PDW
[ "$SIDE" = "front" ] && [ -z "$MOODLE_CRON_PWD" ] && echo -n "Type the Moodle cron password, followed by [ENTER]:" && read MOODLE_CRON_PWD

case $SIDE in
	"front") new_front_tenant;;
	"bd") new_bd_tenant;;
	"*") usage;;
esac
