#!/bin/bash

# ENVIRONMENT VARS EXPECTED (export {VARNAME} into another caller script scope, from outside can be insecure)
#	BASE_DOMAIN			(eg.: mpenolms.com)
#	MOODLE_DB_HOST		(eg.: localhost, {aws-rds-endpoint}...)
#	MOODLE_DB_USER		(eg.: root, moodleuser)
#	MOODLE_DB_USER_PDW
#	AWS_ACCESS_KEY_ID (only mandatory when --env=aws)
#	AWS_SECRET_ACCESS_KEY (only mandatory when --env=aws)
#	NFS_ENDPOINT (only mandatory when --env=aws)
#	SMTP_RELAY_HOST (only mandatory when --env=aws)
#	SMTP_RELAY_PORT (only mandatory when --env=aws)
#	CENTOS_DEV_USER (eg: mpeno for dev, root for aws)
#	S3_BUCKET_CONFS (only mandatory when --env=aws)

# DATA TYPES
ENVIR_TYPE=(dev aws)
BOOL_TYPE=(true false)
INSTALL_MODE_TYPE=(front bd mix)

# CONSTANTS
ENV_VARS=("BASE_DOMAIN" "MOODLE_DB_HOST" "MOODLE_DB_USER" "MOODLE_DB_USER_PDW" "AWS_ACCESS_KEY_ID (if env=aws)" "AWS_SECRET_ACCESS_KEY (if env=aws)" "NFS_ENDPOINT (if env=aws)" "SMTP_RELAY_HOST (if env=aws)" "SMTP_RELAY_PORT (if env=aws)" "CENTOS_DEV_USER")
COMMAND_LINE_OPTS=("--env <dev|aws>" "--install-mode <front|bd|mix>" "[--php-xdebug <true|false>]" "[--http-xforward <true|false>]" "[--s3bucket-apache <string mandatory if env=aws>]")
HOSTNAME=`uname -n`
CURRENTDATE=$(date +'%Y%m%d')
CURRENT_PATH=${PWD}
REPO_CONTENIDOS=https://mpenobarrio@bitbucket.org/moodle_aws/moodle_mt.git
MOODLE_FOLDER_SHARED=/data/cluster_shared
MOODLE_DATA_PATH=$MOODLE_FOLDER_SHARED/moodledatas

# VARIABLES
ENVIR=
INSTALL_MODE=
ENABLE_PHP_XDEBUG=
HTTP_XFORWARD=
S3_BUCKET_CONFS=

# FUNCTIONS
inarray() {
	# Args: 1 = needle, 2 = haystack array, [3 = starts-with or, if not, exact match]
	local e

	if [ "$3" = "starts-with" ] ; then
		for e in "${@:2}"; do
			COND=$(echo "$1" | grep "^$e")
			[ ! "$COND" = "" ] && return "1";
		done
		return "0"
	fi

	# en otro caso
	for e in "${@:2}"; do [ "$e" = "$1" ] && return "1"; done
	return "0"
}
git_clone() {
	# Args: 1 = remote url, 2 = branch, 3 = local path, 4 = is shallow clone or not
	if [ "$4" = "shallow" ] ; then
		git clone -b "$2" --depth 1 "$1" "$3" && cd "$3"
	else
		git clone -b "$2" "$1" "$3" && cd "$3"
	fi
	git config core.fileMode false && git config core.ignorecase false
	[ ! "$?" = "0" ] && exit 1
	cd "$CURRENT_PATH" 
}
set_php_conf(){
	# Args: 1 = key, 2 = value, 3 = file config path
	CONFIG_ITEM="$1" && VALOR="$2" && RUTA="$3"
	NUMS=$(grep -E "^$CONFIG_ITEM[[:blank:]]*=" $RUTA | wc -l | awk '{print $1}')
	LINE=$(grep -n "^;$CONFIG_ITEM[[:blank:]]*=" $RUTA | cut -f1 -d: | head -1)
	LINE_COMODIN=$(grep -En "^\[[^P]" $RUTA | cut -f1 -d: | head -1)
	if [ "$NUMS" -gt "0" ] ; then
		REGEXPR="s/^$CONFIG_ITEM\s*=\s*\([^ ]*\).*$/$CONFIG_ITEM = $VALOR/g"
		sed -i "$REGEXPR" $RUTA
	elif [ ! "$LINE" = "" ] && [ "$LINE" -gt "1" ] ; then
		REGEXPR="${LINE}s/^;$CONFIG_ITEM\s*=\s*\([^ ]*\).*$/$CONFIG_ITEM = $VALOR/"
		sed -i -e "$REGEXPR" $RUTA
	elif [ "$LINE_COMODIN" -gt "1" ] ; then
		LINE_COMODIN=$(( $LINE_COMODIN - 1 ))
		sed -i "${LINE_COMODIN}i $CONFIG_ITEM = $VALOR" $RUTA
	fi
}
set_apache_gral_conf(){
	# Args: 1 = directive, 2 = value, 3 = file config path
	CONFIG_ITEM="$1" && VALOR="$2" && RUTA="$3"
	NUMS=$(grep -E "^$CONFIG_ITEM[[:blank:]]*" $RUTA | wc -l | awk '{print $1}')
	LINE=$(grep -n "^\#[[:blank:]]*$CONFIG_ITEM[[:blank:]]*" $RUTA | cut -f1 -d: | head -1)
	if [ "$NUMS" -gt "0" ] ; then
		REGEXPR="s/^$CONFIG_ITEM\s*\([^ ]*\)$/$CONFIG_ITEM $VALOR/g"
		sed -i "$REGEXPR" $RUTA
	elif [ ! "$LINE" = "" ] && [ "$LINE" -gt "1" ] ; then
		REGEXPR="${LINE}s/^\#$CONFIG_ITEM\s*\([^ ]*\)$/$CONFIG_ITEM $VALOR/"
                sed -i -e "$REGEXPR" $RUTA
	else
		echo "" >> $RUTA && echo "$CONFIG_ITEM $VALOR" >> $RUTA
	fi
}
mandatoriesenvvars(){
	[ "$BASE_DOMAIN" = "" ] && usage "envvars" ; [ "$CENTOS_DEV_USER" = "" ] && usage "envvars"
	[ "$MOODLE_DB_USER" = "" ] && usage "envvars" ; [ "$MOODLE_DB_USER_PDW" = "" ] && usage "envvars" ; [ "MOODLE_DB_USER_PDW" = "" ] && usage "envvars"
	if [ "$ENVIR" = "aws" ] ; then
		[ "$AWS_ACCESS_KEY_ID" = "" ] && usage "envvars" ; [ "$AWS_SECRET_ACCESS_KEY" = "" ] && usage "envvars"
		[ "$NFS_ENDPOINT" = "" ] && usage "envvars" ; [ "$SMTP_RELAY_HOST" = "" ] && usage "envvars" ; [ "$SMTP_RELAY_PORT" = "" ] && usage "envvars"
	fi
}
mandatoriesargs() {
	[ "$ENVIR" = "" ] && usage ; [ "$INSTALL_MODE" = "" ] && usage
	if [ "$ENVIR" = "aws" ] ; then
		[ "$S3_BUCKET_CONFS" = "" ] && usage
	fi
}
usage() {
	# Params $1 = 'cmline' or 'envvars'
	echo "Usage: " 1>&2;
	[ ! "$1" = "envvars" ] && echo "     command line: >$0 ${COMMAND_LINE_OPTS[@]}" 1>&2;
	[ "$1" = "envvars" ] && echo "     env vars: ${ENV_VARS[@]}" 1>&2;
	exit 1
	#echo "-r <repoforge-mirror:string-url>]";
}

# OPTS READING
while [ "$#" -gt 0 ] ; do
	case "$1" in
		--env) ENVIR=$2 ; inarray $ENVIR "${ENVIR_TYPE[@]}" ; [ "$?" = "0" ] && usage;;
		--install-mode) INSTALL_MODE=$2 ; inarray $INSTALL_MODE "${INSTALL_MODE_TYPE[@]}" ; [ "$?" = "0" ] && usage;;
		--php-xdebug) ENABLE_PHP_XDEBUG=$2 ; inarray $ENABLE_PHP_XDEBUG "${ENABLE_PHP_XDEBUG[@]}" ; [ "$?" = "0" ] && usage;;
		--http-xforward) HTTP_XFORWARD=$2 ; inarray $HTTP_XFORWARD "${HTTP_XFORWARD[@]}" ; [ "$?" = "0" ] && usage;;
		--s3bucket-apache) S3_BUCKET_CONFS=$2;;
		*) usage;;
	esac
	# next iter
	shift 2
done
mandatoriesargs ; mandatoriesenvvars

# Selinux
if [ "ENVIR" = "dev" ] ; then
	SELINUX=$(cat /etc/selinux/config | grep -e "^SELINUX=" | sed 's/=/\ /g' | awk '{print $2;}')
	if [ ! "$SELINUX" = "" ] && [ ! "$SELINUX" = "disabled" ] ; then
		sed -i "s/^SELINUX=enforcing/SELINUX=disabled/g" /etc/selinux/config && init 6 && exit
	fi
else
	setenforce 0
fi

# 1) Update and basic tools for admins and developers
yum -y update; yum -y install vim telnet wget zip unzip bzip2 man apachetop git python screen yum-utils
yum install -y epel-release

# Needed for VBoxLinuxAdditions 
[ "$ENVIR" = "dev" ] && yum install -y dkms binutils gcc make patch libgomp glibc-headers glibc-devel kernel-headers kernel-devel

# PRE COMMON TASKS
# Timezone = Madrid
mkdir -p /data/tmp ; mkdir -p /data/logs ; mkdir -p /data/html ; mkdir -p /data/backups
yes | cp -p /etc/localtime /etc/localtime.bak && rm /etc/localtime && ln -s /usr/share/zoneinfo/Europe/Madrid /etc/localtime
timedatectl set-timezone Europe/Madrid
# Git configs
git config --system core.fileMode false ; git config --system core.ignorecase false
git config --global core.fileMode false ; git config --global core.ignorecase false
git config --global user.name "DEMO PFC" ; git config --global user.email fake@fake.com
# MySQL repo
wget http://repo.mysql.com/mysql-community-release-el7-5.noarch.rpm  -O /tmp/mysql-com.rpm
rpm -ivh /tmp/mysql-com.rpm ; rm -rf /tmp/mysql-com.rpm
yum-config-manager --disable mysql5* ; yum-config-manager --enable mysql56-community
# Installing AWS CLI
if ! aws_loc="$(type -p "aws")" || [ -z "$aws_loc" ] ; then
	wget --no-check-certificate https://s3.amazonaws.com/aws-cli/awscli-bundle.zip -O /tmp/awscli-bundle.zip
	cd /tmp ; unzip awscli-bundle.zip
	./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws
	rm -rf /tmp/awscli-bundle.zip && rm -rf /tmp/awscli-bundle
	cd $CURRENT_PATH
fi

# Install 

# If dev environment => Desktop
if [ "$ENVIR" = "dev" ] ; then

	ENABLE_PHP_XDEBUG="true"
	# Install minimal Desktop
	yum -y install gnome-classic-session gnome-terminal nautilus-open-terminal control-center liberation-mono-fonts
	yum groupinstall -y "X Window system"
	yum install -y rpm-build
	yum install -y unique-devel intltool gstreamer-plugins-base-devel gstreamer-devel alsa-lib-devel \
		startup-notification-devel libxml2-devel xfconf-devel xfce4-panel-devel xfce4-panel-devel \
		libxfce4ui-devel libxfce4util-devel
	wget http://dl.fedoraproject.org/pub/epel/7/SRPMS/x/xfce4-mixer-4.10.0-7.el7.src.rpm -O /tmp/xfce4-mixer-4.10.0-7.el7.src.rpm
	rpmbuild --rebuild /tmp/xfce4-mixer-4.10.0-7.el7.src.rpm
	yum install -y rpmbuild/RPMS/x86_64/xfce4-mixer-4.10.0-7.el7.centos.x86_64.rpm
	cd $CURRENT_PATH ; rm -rf /tmp/xfce4-mixer-4.10.0-7.el7.src.rpm ; rm -rf rpmbuild
	yum --enablerepo=epel groupinstall "Xfce" -y
	echo xfce4-session > ~/.xsession && echo xfce4-session > /etc/skel/.xsession
	echo xfce4-session > /home/centos/.xsession && chown centos:centos /home/centos/.xsession
	echo "exec /usr/bin/xfce4-session" > ~/.xinitrc && echo "exec /usr/bin/xfce4-session" > /etc/skel/.xinitrc
	echo "exec /usr/bin/xfce4-session" > /home/centos/.xinitrc && chown centos:centos /home/centos/.xinitrc
	systemctl set-default graphical.target ; systemctl isolate graphical.target

	# Enable ssh connections by password
	set_apache_gral_conf PasswordAuthentication yes /etc/ssh/sshd_config
	systemctl restart sshd.service
	
	# Prompt variables
	echo -n "You must define your LINUX user [default 'mpeno']: " ; read LUSER ; [ ! "$LUSER" = "" ] && CENTOS_DEV_USER=$LUSER
	echo -n "You must define a new password for $CENTOS_DEV_USER: " ; passwd $CENTOS_DEV_USER
	echo -n "You must define a new base domain for your multi tenant site: " ; read BASEDOM ; [ ! "$BASEDOM" = "" ] && BASE_DOMAIN=$BASEDOM
		
	# Sw for remote Desktop management
	yum -y install xrdp tigervnc-server
	chcon --type=bin_t /usr/sbin/xrdp
	chcon --type=bin_t /usr/sbin/xrdp-sesman
	firewall-cmd --permanent --zone=public --add-port=3389/tcp && firewall-cmd --reload
	systemctl start xrdp ; systemctl enable xrdp
	service xrdp restart
	
	# Installing Firefox, Chrome and Java
	yum install -y firefox java-1.8.0-openjdk
	wget https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm -O /tmp/chrome.rpm
	yum install -y /tmp/chrome.rpm
	
	# Installing Eclipse
	wget --no-check-certificate http://mirror.ibcp.fr/pub/eclipse//technology/epp/downloads/release/neon/3/eclipse-java-neon-3-linux-gtk-x86_64.tar.gz -O /tmp/eclipse.tar.gz
	tar -zxvf /tmp/eclipse.tar.gz -C /opt/ ; rm -rf /tmp/eclipse.tar.gz
	ln -s /opt/eclipse/eclipse /usr/bin/eclipse
	ECL_DESKTOP_ENTRY="[Desktop Entry]\nEncoding=UTF-8\nName=Eclipse 4.6.3\nComment=Eclipse Neon.3\nExec=/usr/bin/eclipse\nIcon=/opt/eclipse/icon.xpm\nCategories=Application;Development;Java;IDE\nVersion=1.0\nType=Application\nTerminal=0"
	echo -en $ECL_DESKTOP_ENTRY > /usr/share/applications/opt_eclipse.desktop

fi

# N) Installing Front Sw (Apache + PHP + Moodle adapted for multi-tenancy) if --install-mode option is front or mix
if [ "$INSTALL_MODE" = "front" ] || [ "$INSTALL_MODE" = "mix" ] ; then
	
	# Apache
	yum install -y epel-release ; yum install -y httpd mod_ssl nodejs
	mkdir -p /data/logs/php ; chown -R apache:root /data/logs/php ; chmod -R 770 /data/logs/php
	mkdir -p /data/logs/apache ; chown -R apache:root /data/logs/apache ; chmod -R 750 /data/logs/apache
	TMPPATH=/etc/httpd/conf.d/welcome.conf
	[ -f "$TMPPATH.orig" ] && cp -p $TMPPATH $TMPPATH.orig; sudo sed -i 's/^/#&/g' $TMPPATH
	TMPPATH=/etc/httpd/conf/httpd.conf
	[ -f "$TMPPATH.orig" ] && cp -p $TMPPATH $TMPPATH.orig; sed -i "s/Options Indexes FollowSymLinks/Options FollowSymLinks/" $TMPPATH
	sed -i 's/^#ServerName /ServerName /g' $TMPPATH
	set_apache_gral_conf "ServerName" "localhost" "$TMPPATH" ; set_apache_gral_conf "ServerTokens" "Prod" "$TMPPATH" ;
	set_apache_gral_conf "ServerSignature" "Off" "$TMPPATH" ; set_apache_gral_conf "LogLevel" "warn" "$TMPPATH"
	[ "$ENVIR" = "aws" ] && set_apache_gral_conf "LogLevel" "error" "$TMPPATH"
	if [ "$HTTP_XFORWARD" = "true" ] ; then
		echo "" >> $TMPPATH && echo "#Behind balancer => 'combined_for' with X-Forward" >> $TMPPATH && echo "LogFormat \"%{X-Forwarded-For}i %l %u %t \\\"%r\\\" %>s %b \\\"%{Referer}i\\\" \\\"%{User-Agent}i\\\"\" combined_for" >> $TMPPATH
	else
		echo "" >> $TMPPATH && echo "#'combined_for' with %h" >> $TMPPATH && echo "LogFormat \"%h %l %u %t \\\"%r\\\" %>s %b \\\"%{Referer}i\\\" \\\"%{User-Agent}i\\\"\" combined_for" >> $TMPPATH
	fi	
	
	mkdir -p /etc/httpd/conf.d/certs ; chmod 644 /etc/httpd/conf.d/certs
	openssl req -x509 -nodes -newkey rsa:2048 -keyout /etc/httpd/conf.d/certs/mysitename.key -out /etc/httpd/conf.d/certs/mysitename.crt -subj "/C=ES/ST=Madrid/L=Madrid/O=Manuel/OU=IT/CN=*.$BASE_DOMAIN"
	
	[ -f "/etc/httpd/conf.d/ssl.conf.orig" ] && cp -p /etc/httpd/conf.d/ssl.conf /etc/httpd/conf.d/ssl.conf.orig
	yes | cp $CURRENT_PATH/templates/apache/ssl.conf /etc/httpd/conf.d/ssl.conf
	
	# PHP
	wget http://rpms.famillecollet.com/enterprise/remi-release-7.rpm -O /tmp/remi.rpm
	rpm -Uvh /tmp/remi.rpm ; rm -rf /tmp/remi.rpm
	yum-config-manager --disable remi* ; yum-config-manager --enable remi ; yum-config-manager --enable remi-php56
	yum -y install php ; yum -y update php
	yum -y install php-gd php-mysql php-mcrypt php-mbstring php-xmlrpc php-soap php-intl php-opcache
	yum -y update php-gd php-mysql php-mcrypt php-mbstring php-xmlrpc php-soap php-intl php-opcache
	#sed -i 's/opcache.enable=\([^ ]*\)$/opcache.enable=0/g' /etc/php.d/opcache.ini
	yum -y install php-pear php-devel gcc curl-devel zlib-devel pcre-devel
	pecl install xdebug ; chmod 755 /usr/lib64/php/modules/xdebug.so
	[ -f "/etc/php.ini.orig" ] && cp -p /etc/php.ini /etc/php.ini.orig
	[ "$ENABLE_PHP_XDEBUG" = "true" ] && yes | cp $CURRENT_PATH/templates/php/90-xdebug.ini /etc/php.d; chmod 644 /etc/php.d/90-xdebug.ini
	[ ! "$ENABLE_PHP_XDEBUG" = "true" ] && yes | cp $CURRENT_PATH/templates/php/90-xdebug.ini /etc/php.d/90-xdebug.ini.disab; chmod 644 /etc/php.d/90-xdebug.ini.disab
	
	TMPPATH=/etc/php.ini ; [ -f "$TMPPATH" ] && [ ! -f "${TMPPATH}.orig" ] && cp -p $TMPPATH $TMPPATH.orig
	set_php_conf "expose_php" "Off" $TMPPATH ; set_php_conf "session.save_handler" "files" $TMPPATH ; set_php_conf "session.auto_start" "0" $TMPPATH
    set_php_conf "file_uploads" "On" $TMPPATH ; set_php_conf "max_execution_time" "480" $TMPPATH ; set_php_conf "max_input_vars" "8000" $TMPPATH
    set_php_conf "memory_limit" "256M" $TMPPATH ; set_php_conf "post_max_size" "2048M" $TMPPATH ; set_php_conf "upload_max_filesize" "2048M" $TMPPATH
    set_php_conf "log_errors" "On" $TMPPATH ; set_php_conf "error_log" "\/var\/log\/php_errors\.log" $TMPPATH ; set_php_conf "date\.timezone" "Europe\/Madrid" $TMPPATH
	[ "$ENVIR" = "dev" ] && set_php_conf "display_errors" "On" $TMPPATH && set_php_conf "display_startup_errors" "On" $TMPPATH && set_php_conf "track_errors" "On" $TMPPATH
	
	BASE_DOMAIN_SLASH=$(echo "$BASE_DOMAIN" | sed 's/\./\\./g')
	TMPPATH=/etc/hosts
	ISDOMAIN=$(grep -P "^127\.0\.0\.1.+$.*tenant1\.$BASE_DOMAIN_SLASH$" $TMPPATH | wc -l | awk '{print $1}')
	[ "$ISDOMAIN" = "0" ] &&  echo "" >> $TMPPATH && echo "127.0.0.1 tenant1.$BASE_DOMAIN" >> $TMPPATH
	ISDOMAIN=$(grep -P "^127\.0\.0\.1.+$.*tenant2\.$BASE_DOMAIN_SLASH$" $TMPPATH | wc -l | awk '{print $1}')
	[ "$ISDOMAIN" = "0" ] &&  echo "" >> $TMPPATH && echo "127.0.0.1 tenant2.$BASE_DOMAIN" >> $TMPPATH

	TMPPATH=/data/html/moodle_mt
	mkdir -p $TMPPATH ; git_clone "$REPO_CONTENIDOS" "master" "$TMPPATH"
	yes | cp $CURRENT_PATH/templates/moodle/hooks/* /data/html/moodle_mt/.git/hooks
	sed -i 's/\[linux\-user\]/${CENTOS_DEV_USER}/g' /data/html/moodle_mt/.git/hooks/*
	chown -R apache:$CENTOS_DEV_USER $TMPPATH ; chmod -R 770 $TMPPATH
	TMPPATH=/data/html/moodle_mt
	yes | cp -p $TMPPATH/config-moodlemt-template.php $TMPPATH/config.php
	MOODLE_DATA_PATH_ESC=$(echo "$MOODLE_DATA_PATH" | sed "s/\//\\\\\//g")
	sed -i "s/\[moodedatas\-path\]/${MOODLE_DATA_PATH_ESC}/g" $TMPPATH/config.php ; sed -i "s/\[db\-host\]/${MOODLE_DB_HOST}/g" $TMPPATH/config.php
	sed -i "s/\[db\-user\]/${MOODLE_DB_USER}/g" $TMPPATH/config.php ; sed -i "s/\[db\-user\-pass\]/${MOODLE_DB_USER_PDW}/g" $TMPPATH/config.php
	TMPPATH=$MOODLE_DATA_PATH
	mkdir -p $TMPPATH ; chown -R apache:root $TMPPATH ; chmod -R 770 $TMPPATH
	
	# Mounts shared NFS for moodledatas
	if [ "$ENVIR" = "aws" ] ; then
		yum install -y nfs-utils
		FIRSTOCCUR=$(cat /etc/fstab | grep -n "$MOODLE_DATA_PATH" | cut -f1 -d: | head -1)
		if [ "$FIRSTOCCUR" = "" ] ; then
			[ ! -f "/etc/fstab.$CURRENTDATE" ] && cp -p /etc/fstab /etc/fstab.$CURRENTDATE
			echo -e "\n$NFS_ENDPOINT:/ $MOODLE_DATA_PATH nfs4 nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 0 0\n" >> /etc/fstab
			mount -a
		fi
	fi
	cd $CURRENT_PATH
	
	# Installing Amazon SSM agent (instance must be created with a Systems Manager IAM Role)
	# 	https://docs.aws.amazon.com/systems-manager/latest/userguide/sysman-configuring-access-policies.html
	if [ "$ENVIR" = "aws" ] ; then
		yum install -y https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/linux_amd64/amazon-ssm-agent.rpm
		systemctl enable amazon-ssm-agent
	fi

	# MySQL client
	yum -y install mysql-community-client
	
	# Maintenance curtain + Default sample tenants
	if [ "$ENVIR" = "dev" ] ; then
		TMPPATH=/etc/httpd/conf.d/00-apache_cross_maintenance.conf.hidden
		yes | cp $CURRENT_PATH/templates/apache/00-apache_cross_maintenance.conf.hidden /etc/httpd/conf.d ; chmod 644 $TMPPATH
		BASE_DOMAIN_ESC=$(echo "$BASE_DOMAIN" | sed "s/\./\\\./g")
		BASE_DOMAIN_ESC2=$(echo $BASE_DOMAIN | sed 's/\./\\\\\./g')
		sed -i 's/\[port\]/443/g' $TMPPATH ; sed -i "s/\[basedomain\]/$BASE_DOMAIN_ESC/g" $TMPPATH
		# Port 80 not allowed, so redirect to 443
		TMPPATH=/etc/httpd/conf.d/01.${BASE_DOMAIN}.80.conf
		yes | cp $CURRENT_PATH/templates/apache/XX.domain80.conf $TMPPATH ; chmod 644 $TMPPATH
		sed -i "s/\[domain\]/$BASE_DOMAIN_ESC/g" $TMPPATH ; sed -i "s/\[domain\_slash\]/$BASE_DOMAIN_ESC2/g" $TMPPATH
		$CURRENT_PATH/create_tenant.sh -s front -i defaultblank -b $BASE_DOMAIN -h false
		$CURRENT_PATH/create_tenant.sh -s front -i tenant1 -b $BASE_DOMAIN -h false
		$CURRENT_PATH/create_tenant.sh -s front -i tenant2 -b $BASE_DOMAIN -h false
	fi
	if [ "$ENVIR" = "aws" ] ; then
		aws s3 sync s3://$S3_BUCKET_CONFS/apache_confd /etc/httpd/conf.d --only-show-errors
		WITHMOODLEDATAS=$(grep -rln "443" /etc/httpd/conf.d | grep -v "maintenance" | grep -E "\/[0-9]{2,5}")
		printf '%s\n' "$WITHMOODLEDATAS" | while IFS= read -r line
		do
		   DOMIN=$(grep "ServerName" $line | awk '{print $2}') ; DOMIN=$(echo "" | sed 's/\./\_/g' | sed 's/\-/\__/g')
		   mkdir -p $MOODLE_DATA_PATH/$DOMIN && chown apache:root $MOODLE_DATA_PATH/$DOMIN && chmod 770 $MOODLE_DATA_PATH/$DOMIN
		done
	fi
	
	# Sen mail relay (only for $ENVIR = aws). AWS 
	if [ "$ENVIR" = "aws" ] ; then
		yum install -y postfix nail
		# TODO Si estamos en "$ENVIR" = "aws" isntalar postfix y configurar relay hacia AWS SES (OJO con limitacion del puerto 25, ver como IO lo hace en S2S Fundacion para usar otro puerto)
		# OJO, como puerto 25 esta capado, tratar de usar el 2525 o 
	fi
	
	# Last step = starting Apache
	systemctl start httpd.service
	systemctl enable httpd.service
fi

if [ "$INSTALL_MODE" = "bd" ] || [ "$INSTALL_MODE" = "mix" ] ; then
	
	echo -n "You must define the MySQL root user password: " && read DB_ROOT_PWD
	echo -n "You must define the MySQL moodleuser DB netmask (eg: 10.0.%): " && read BD_MOODLEUSER_NETMASK

	# MySQL Server
	yum -y install mysql-server ; systemctl start mysqld.service ; systemctl enable mysqld.service
	# NOTE: For development purpose only. In production, the BD must be securized.
	
	# Two example tenants
	SQL_NETMASK=localhost
	[ "$ENVIR" = "dev" ] && $CURRENT_PATH/create_tenant.sh -s bd -i defaultblank -b $BASE_DOMAIN -n $SQL_NETMASK
	[ "$ENVIR" = "dev" ] && $CURRENT_PATH/create_tenant.sh -s bd -i tenant1 -b $BASE_DOMAIN -n $SQL_NETMASK
	[ "$ENVIR" = "dev" ] && $CURRENT_PATH/create_tenant.sh -s bd -i tenant2 -b $BASE_DOMAIN -n $SQL_NETMASK
fi

#Post manual steps when $ENVIR = "dev":
#  - Install VBoxLinuxAdditions in VirtualBox guest: yoy must mount the ISO and execute ./VBoxLinuxAdditions.sh
#  - Install NetBeans for PHP when GUI Desktop (Gnome) available:
#		wget http://download.netbeans.org/netbeans/8.2/final/bundles/netbeans-8.2-php-linux-x64.sh -O /tmp/netbeans.sh
#		chmod +x /tmp/netbeans.sh ; sh /tmp/netbeans.sh ; rm -rf /tmp/netbeans.sh
#  - TODO Disabling updates for eclipse, netbeans, firefox, chrome and java
#  - To launch Firefox and Netbeans when session starts:
#		- Login with user mpeno:
#			#xfce4-session-settings
#			>gnome-session-properties
#				Add new custom launch: firefox %u
#				Add new custom launch: /bin/sh "/usr/local/netbeans-8.2/bin/netbeans"
#  - TODO Anadir el sitio como de confianza en firefox y chrome
#  - TODO Que no se bloquee la pantalla tras mucho tiempo (Aplicaciones > Herramientas del Sistema > Configuracion > Energia)	

yum clean all